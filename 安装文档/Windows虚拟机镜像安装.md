 **此方式安装可在Windows系统上完成，比较简单可靠，主要用于初次上手体验系统功能** 

1. 安装virtual box
  创建一个空的用于存放虚拟机的空目录
  安装virtual box指定上述目录
  
2. 安装erpnext
  
- 下载ERPnext镜像文件 https://erpnext.org/download 或 https://erpnext.org/get-started
-   在virtual box里导入虚拟机文件

  
  
3. 启动ERPNext

-   在virtualbox中启动erpnext虚拟机
-   在虚拟机命令窗口中用帐号frappe,密码frappe登录
-   切换到frappe-bench目录 `cd frappe-bench`
-   安装中文件本地化应用	
```
bench get-app https://gitee.com/yuzelin/ebclocal.git
bench install-app ebclocal
```

-   启动ERPNext：如果是开发镜像，运行命令 `bench start`，生产机镜像 `supervisorctl start all`



4. 打开浏览器，输入网址 localhost:8000(默认网络连接方式NAT）
  用户名administrator，密码 admin

  因为在虚拟机终端输入命令不支持鼠标及键盘的命令复制粘贴，建议将网络连接方式改为桥接，
  ![网络方式改为桥接](https://images.gitee.com/uploads/images/2020/1112/223524_33a26249_7624863.png "屏幕截图.png")
  在虚拟机命令窗口中输入ifconfig，找出192.168.开头的网址，网页可用此IP访问，
  ![ifconfig 查虚拟机IP](https://images.gitee.com/uploads/images/2020/1112/223616_0ec5532b_7624863.png "屏幕截图.png") 

  可在windows的CMD窗口或其它SSH 工具如putty中通过以下SSH命令连接
`SSH frappe@192.168.3.67`

![SSH连接](https://images.gitee.com/uploads/images/2020/1112/223432_cd1f14ad_7624863.png "屏幕截图.png")

常见问题
1. bench start 提示 watch file数不够
   原因：系统要监控应用目录下所有文件的变动，有变动就自动重启应用，适用于开发环境，而包括的文件数太多，系统配置的最大文件数不够
   解决方案：先ctrl+c退出`bench start`，执行以下命令修改相关参数，再重新运行`bench start`
		`echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sudo sysctl -p`

2. 网页第一次初始化最后提示setup 失败
   原因：第一次运行，系统会编译python代码文件，编译动作导致系统不断刷新重启
   解决方案：点网页上的重试按钮多次

3. 想更换下载后的版本
    可使用bench update或
    bench switch-to-branch命令
    更多 bench 命令 可输入 bench --help
